from bottle import template

from indicators.fair_value import *
from indicators.mayer_multiple import *
from indicators.mm20_weekly_support import *
from indicators.pi_indicator import *
from indicators.transaction_fees import *
from indicators.ath_delta import *
from indicators.wma200_heatmap import *
from indicators.stock_2_flow import *
from indicators.price_temperature import *
from flask import *

app = Flask(__name__)


@app.route('/')
def home():
    usd_fees = get_usd_fees()
    usd_fees_color = get_fees_usd_color(usd_fees)
    usd_fees_bg_colors = [blue, usd_fees_color]

    btc_fees = get_btc_fees()
    btc_fees_color = get_fees_btc_color(btc_fees)
    btc_fees_bg_colors = [blue, btc_fees_color]

    mayer_multiple = get_mayer_multiple()
    mayer_color = [blue, get_mayer_multiple_color(mayer_multiple)]

    ath_delta = get_ath_delta()
    ath_delta_color = [purple, get_ath_delta_color(ath_delta)]

    btc_current_value = get_latest_btc_price()
    mm20 = get_mm20_weekly()
    mm20_color = [get_mm20_weekly_color(mm20), purple]

    pi = get_pi_indicator()
    pi_color = [get_pi_indicator_color(pi)]

    wma200 = get_wma200_heatmap()
    wma200_color = [get_wma200_heatmap_color(wma200)]

    stock_2_flow_463 = get_stock_2_flow(463)
    stock_2_flow_10 = get_stock_2_flow(10)
    stock_2_flow_colors = get_stock_2_flow_colors(btc_current_value, stock_2_flow_10, stock_2_flow_463)

    price_temperature = get_price_temperature()
    price_temperature_color = [get_price_temperature_color(price_temperature)]

    return template('main.tpl',
                    usd_fees_type="'bar'", usd_fees_labels=['2017 high', 'Now'], usd_fees_label="'BTC transaction fees USD'", usd_fees_data=[55, usd_fees], usd_fees_bgColor=usd_fees_bg_colors, usd_fees_borderColor=usd_fees_bg_colors,
                    btc_fees_type="'bar'", btc_fees_labels=['2017 high', 'Now'], btc_fees_label="'BTC transaction fees Satoshi'", btc_fees_data=[275000, btc_fees], btc_fees_bgColor=btc_fees_bg_colors, btc_fees_borderColor=btc_fees_bg_colors,
                    mayer_type="'bar'", mayer_labels=['2017 high', 'Now'], mayer_label="'Mayer multiple'", mayer_data=[3.8, mayer_multiple], mayer_bgColor=mayer_color, mayer_borderColor=mayer_color,
                    ath_delta_type="'bar'", ath_delta_labels=['ATH', 'Now'], ath_delta_label="'ATH delta'", ath_delta_data=[36, ath_delta*100], ath_delta_bgColor=ath_delta_color, ath_delta_borderColor=ath_delta_color,
                    mm20_type="'bar'", mm20_labels=['BTC value', 'MM20 weekly support'], mm20_label="'MM20 weekly support'", mm20_data=[btc_current_value, mm20], mm20_bgColor=mm20_color, mm20_borderColor=mm20_color,
                    pi_type="'pie'", pi_labels=['PI'], pi_label="'PI indicator'", pi_data=[pi], pi_bgColor=pi_color, pi_borderColor=pi_color,
                    wma200_type="'pie'", wma200_labels=['200 Week Moving Average Heatmap'], wma200_label="'200 Week Moving Average Heatmap'", wma200_data=[wma200], wma200_bgColor=wma200_color, wma200_borderColor=wma200_color,
                    pt_type="'pie'", pt_labels=['Price temperature'], pt_label="'Price temperature'", pt_data=[price_temperature], pt_bgColor=price_temperature_color, pt_borderColor=price_temperature_color,
                    stock_2_flow_type="'bar'", stock_2_flow_labels=['BTC price', 'S2F 10', 'S2F 463'], stock_2_flow_label="'Stock to flow price in USD'", stock_2_flow_data=[btc_current_value, stock_2_flow_10, stock_2_flow_463], stock_2_flow_bgColor=stock_2_flow_colors, stock_2_flow_borderColor=stock_2_flow_colors)


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True, port=8080)
