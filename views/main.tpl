<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Take profit indicators</title>
    <script src="/static/Chart.bundle.js"></script>
</head>
<body>

<script async src="http://platform.twitter.com/widgets.js" charset="utf-8"></script>

<center>
<table style="font-family:verdana;" cellpadding=5 cellspacing=5>
    <tr>
        <td width="20%" rowspan=20>
            Site de willy woo : <a target="_blank" href="https://willywoo.substack.com">here!</a>
            <a target="_blank" class="twitter-timeline" href="https://twitter.com/woonomic" data-tweet-limit="8" data-width="500" ></a>
        <td/>
        <td>
        Le code couleur des indicateurs se présente <br/>comme tel :
        <ul>
            <li>Bleu : valeur de référence</li>
            <li>Violet : valeur de référence mobile</li>
            <li>Vert : tout va bien</li>
            <li>Jaune : indicateur à surveiller</li>
            <li>Orange : on se rapproche de la valeur critique</li>
            <li>Rouge : cours !</li>
        </ul>
        </td>
    </tr>
    <tr>
    <td>
    <tr>
        <td><canvas id="transactionFeesUsd" width="300" height="300"></canvas></td>
        <td><h3>Frais de transaction en USD sur blockchain Bitcoin</h3>
        Les frais de transaction ont tendance à s'élever fortement lorsque l'on arrive en fin de bull market. <br/>Cet indicateur permet de connaitre le prix que sont prêts à mettre les gens pour que leur transaction passe vite : <br/>cela reflète le comportement des particuliers qui FOMO et des professionels qui sortent.<br/>
        Lien : <a target="_blank" href="https://bitinfocharts.com/comparison/bitcoin-transactionfees.html">courbe des frais de transaction sur BTC</a>
        </td>
    </tr>
    <tr>
        <td><canvas id="transactionFeesBtc" width="300" height="300"></canvas></td>
        <td><h3>Frais de transaction en Satoshis sur blockchain Bitcoin</h3>
        Cet indicateur est intéressant pour les mêmes raisons que le précédent. <br/>
        En exprimant les frais en Satoshis, cela permet de comparer la hausse des frais sans prendre en compte la valeur de l'USD face au BTC.
        </td>
    </tr>
    <tr>
        <td><canvas id="mayerMultiple" width="300" height="300"></canvas></td>
        <td><h3>Multiple de Mayer</h3>
        Compris entre 0 et l'infini, mais par observation sa valeur ne dépasse que très rarement les 3.5 et ce... en fin de bullrun. <br/>
        Il suffit de diviser le prix actuel par la moyenne mobile daily 200 périodes.<br/>
        Liens : <a target="_blank" href="https://www.cointribune.com/actualites/trading-de-bitcoin-btc-multiple-de-mayer-predire-les-sommets-de-marche/">Tout est expliqué ici :)</a><br/>
        <a target="_blank" href="https://mayermultiple.info"/>Mayer multiple</a>
        </td>
    </tr>
    <tr>
        <td><canvas id="athDelta" width="300" height="300"></canvas></td>
        <td><h3>Delta avec l'ATH</h3>
        C'est tout simplement l'ampleur du dernier retracement. Lors du dernier bullrun, le retracement temporaire maximum était de ~35%. <br/>
        Le premier retracement qui fut plus important marqua la fin du bullrun.
        </td>
    </tr>
    <tr>
        <td><canvas id="mm20Delta" width="300" height="300"></canvas></td>
        <td><h3>Support moyenne mobile 20</h3>
        En corrélation avec l'indicateur précédent, la moyenne mobile weekly 20 périodes a toujours été le support principal lors de la montée. <br/>
        Une cassure de ce support à la baisse marqua la fin du bullrun.
        </td>
    </tr>
    <tr>
        <td><canvas id="s2f" width="300" height="300"></canvas></td>
        <td><h3>Stock to flow</h3>
        Le 'Stock to flow' est le nombre d'années sont requises, avec le ratio actuel de production, <br/>
        pour atteindre le stock total courant. Plus le nombre est grand, plus le prix sera grand. <br/>
        Un calcul savant est appliqué au S2F pour obtenir le prix correspondant, celui utilisé dans le graphique.<br/>
        Lien : <a target="_blank" href="https://digitalik.net/btc/">Stock to flow chart</a>
        </td>
    </tr>
    <tr>
        <td><canvas id="pi" width="300" height="300"></canvas></td>
        <td><h3>Indicateur des cycles PI</h3>
        Cet indicateur se marque par le croisement des moyennes mobiles daily 111 périodes et daily 350 périodes multipliée par 2. <br/>
        Il porte ce nom car 350/111 ~= 3.14.<br/>
        Depuis le début de l'existance du Bitcoin, à chaque fois que la MM111 passe au dessus de la MM350*2, l'instant du croisement marque l'ATH long terme. <br/>
        Cet indicateur est l'un des plus forts de cette série. Il a également l'avantage d'être précis à l'instant où il croise bearish (pas de latence).<br/>
        Lien : <a target="_blank" href="https://www.lookintobitcoin.com/charts/pi-cycle-top-indicator/">Courbe avec indicateur PI</a>
        </td>
    </tr>
    <tr>
        <td><canvas id="wma200" width="300" height="300"></canvas></td>
        <td><h3>Écart moyenne mobile weekly 200 périodes</h3>
        C'est le delta entre la moyenne mobile weekly 200 périodes actuelle et celle d'il y a une semaine. Si l'écart entre ces deux est inférieur à 10%, c'est okay.<br/>
        Au delà de 14%, on se rapproche de l'ATH long terme.<br/>
        Lien : <a target="_blank" href="https://www.lookintobitcoin.com/charts/200-week-moving-average-heatmap/">Graphique avec indicateur</a>
        </td>
    </tr>
    <tr>
        <td><canvas id="pt" width="300" height="300"></canvas></td>
        <td><h3>Température de prix</h3>
        Cet indicateur permet de voir la distance entre le prix actuel et la moyenne mobile sur 4 ans.<br/>
        La valeur indiquée sera par exemple : prix actuel = 1000, mm = 500 -> valeur = 2<br/>
        Cet indicateur montre qu'au delà de 8, le prix a tendance à chuter fortement et rapidement. Il est semblable au <a href="https://www.lookintobitcoin.com/charts/mvrv-zscore/">MVRV-Z score</a><br/>
        Lien : <a target="_blank" href="https://medium.com/coinmonks/bitcoin-price-temperature-bands-d17695e164ea">Graphique avec indicateur</a>
        </td>
    </tr>
    </td>
    </tr>
</table>
</center>

<script>
new Chart(document.getElementById('transactionFeesUsd').getContext('2d'), {
    type: {{ !usd_fees_type }},
    data: {
        labels: {{ !usd_fees_labels }},
        datasets: [{
            data: {{ !usd_fees_data }},
            label: {{ !usd_fees_label }},
            backgroundColor: {{ !usd_fees_bgColor }},
            borderColor: {{ !usd_fees_borderColor }},
            borderWidth: 1
        }]
    },
    options : {
        responsive: false,
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});


new Chart(document.getElementById('transactionFeesBtc').getContext('2d'), {
    type: {{ !btc_fees_type }},
    data: {
        labels: {{ !btc_fees_labels }},
        datasets: [{
            data: {{ !btc_fees_data }},
            label: {{ !btc_fees_label }},
            backgroundColor: {{ !btc_fees_bgColor }},
            borderColor: {{ !btc_fees_borderColor }},
            borderWidth: 1
        }]
    },
    options : {
        responsive: false,
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});


new Chart(document.getElementById('mayerMultiple').getContext('2d'), {
    type: {{ !mayer_type }},
    data: {
        labels: {{ !mayer_labels }},
        datasets: [{
            data: {{ !mayer_data }},
            label: {{ !mayer_label }},
            backgroundColor: {{ !mayer_bgColor }},
            borderColor: {{ !mayer_borderColor }},
            borderWidth: 1
        }]
    },
    options : {
        responsive: false,
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});


new Chart(document.getElementById('athDelta').getContext('2d'), {
    type: {{ !ath_delta_type }},
    data: {
        labels: {{ !ath_delta_labels }},
        datasets: [{
            data: {{ !ath_delta_data }},
            label: {{ !ath_delta_label }},
            backgroundColor: {{ !ath_delta_bgColor }},
            borderColor: {{ !ath_delta_borderColor }},
            borderWidth: 1
        }]
    },
    options : {
        responsive: false,
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});


new Chart(document.getElementById('mm20Delta').getContext('2d'), {
    type: {{ !mm20_type }},
    data: {
        labels: {{ !mm20_labels }},
        datasets: [{
            data: {{ !mm20_data }},
            label: {{ !mm20_label }},
            backgroundColor: {{ !mm20_bgColor }},
            borderColor: {{ !mm20_borderColor }},
            borderWidth: 1
        }]
    },
    options : {
        responsive: false,
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

new Chart(document.getElementById('pi').getContext('2d'), {
    type: {{ !pi_type }},
    data: {
        labels: {{ !pi_labels }},
        datasets: [{
            data: {{ !pi_data }},
            label: {{ !pi_label }},
            backgroundColor: {{ !pi_bgColor }},
            borderColor: {{ !pi_borderColor }},
            borderWidth: 1
        }]
    }
});

new Chart(document.getElementById('wma200').getContext('2d'), {
    type: {{ !wma200_type }},
    data: {
        labels: {{ !wma200_labels }},
        datasets: [{
            data: {{ !wma200_data }},
            label: {{ !wma200_label }},
            backgroundColor: {{ !wma200_bgColor }},
            borderColor: {{ !wma200_borderColor }},
            borderWidth: 1
        }]
    }
});

new Chart(document.getElementById('pt').getContext('2d'), {
    type: {{ !pt_type }},
    data: {
        labels: {{ !pt_labels }},
        datasets: [{
            data: {{ !pt_data }},
            label: {{ !pt_label }},
            backgroundColor: {{ !pt_bgColor }},
            borderColor: {{ !pt_borderColor }},
            borderWidth: 1
        }]
    }
});


new Chart(document.getElementById('s2f').getContext('2d'), {
    type: {{ !stock_2_flow_type }},
    data: {
        labels: {{ !stock_2_flow_labels }},
        datasets: [{
            data: {{ !stock_2_flow_data }},
            label: {{ !stock_2_flow_label }},
            backgroundColor: {{ !stock_2_flow_bgColor }},
            borderColor: {{ !stock_2_flow_borderColor }},
            borderWidth: 1
        }]
    },
    options : {
        responsive: false,
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});



</script>


</body>
</html>