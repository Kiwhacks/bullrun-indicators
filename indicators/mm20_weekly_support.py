from indicators.colors import *
from indicators.tools import *


def get_mm20_weekly():
    today = datetime.today()
    begin = datetime.fromtimestamp(today.timestamp() - 60 * 60 * 24 * 7 * 20)
    mm20 = get_moving_average(begin.strftime('%Y-%m-%d'), today.strftime('%Y-%m-%d'))
    return mm20


def get_mm20_weekly_color(mm20):
    now = get_latest_btc_price()
    if now/mm20 > 1.05:
        return green
    if now/mm20 > 1:
        return yellow
    if now/mm20 > 0.95:
        return orange
    return red
