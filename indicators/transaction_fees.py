import requests
from bs4 import BeautifulSoup
from indicators.colors import *


def get_usd_fees():
    url = "https://bitinfocharts.com/comparison/bitcoin-transactionfees.html"
    content = requests.get(url).text
    soup = BeautifulSoup(content, features="html.parser")
    for b in soup.find_all("span"):
        if "$" in str(b.string):
            return float(b.string.split()[0][2:])

    return 0


def get_btc_fees():
    url = "https://bitinfocharts.com/comparison/bitcoin-transactionfees.html"
    content = requests.get(url).text
    soup = BeautifulSoup(content, features="html.parser")
    for b in soup.find_all("span"):
        if "BTC" in str(b.string):
            return float(b.string.split()[0]) * 100000000

    return 0


def get_fees_usd_color(fees):
    color = green
    if fees >= 50:
        color = red
    elif fees >= 40:
        color = orange
    elif fees >= 30:
        color = yellow
    return color


def get_fees_btc_color(fees):
    color = green
    if fees >= 275000:
        color = red
    elif fees >= 200000:
        color = orange
    elif fees >= 100000:
        color = yellow
    return color

