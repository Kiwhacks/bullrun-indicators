from indicators.tools import *
from indicators.colors import *


def get_ath_delta():
    return 1-get_latest_btc_price()/get_btc_ath()


def get_ath_delta_color(delta):
    if delta < 0.2:
        return green
    if delta < 0.3:
        return yellow
    if delta < 0.36:
        return orange
    return red
