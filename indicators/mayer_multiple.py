from indicators.tools import *
from datetime import datetime
from indicators.colors import *


def get_mayer_multiple():
    today = datetime.today()
    begin = datetime.fromtimestamp(today.timestamp()-60*60*24*200)
    mm200 = get_moving_average(begin.strftime('%Y-%m-%d'), today.strftime('%Y-%m-%d'))
    actual_price = get_latest_btc_price()
    return actual_price / mm200


def get_mayer_multiple_color(mayer_multiple):
    if mayer_multiple > 3.2:
        return red
    if mayer_multiple > 2.6:
        return orange
    if mayer_multiple > 2.4:
        return yellow
    return green
