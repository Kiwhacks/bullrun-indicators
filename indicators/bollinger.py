from indicators.tools import *
from indicators.colors import *


def get_bollinger_value():
    today = datetime.today()
    begin = datetime.fromtimestamp(today.timestamp()-60*60*24*30*20)
    mm_20_m = get_moving_average(begin.strftime('%Y-%m-%d'), today.strftime('%Y-%m-%d'))
