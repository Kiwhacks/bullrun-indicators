from indicators.tools import *


# https://github.com/ivanacostarubio/mayer-multiple/blob/master/src/main.rs
def get_mvrv_z_score():  # https://medium.com/swlh/introducing-the-bitcoin-price-z-score-edd3f80b7bf7
    btc_market_cap = get_btc_market_cap()
    return (btc_market_cap - get_btc_realized_cap()) / get_stddev(btc_market_cap)
