import requests
import json
from bs4 import BeautifulSoup
from datetime import datetime


def get_moving_average(start, end):
    url = "https://api.coindesk.com/v1/bpi/historical/close.json?start=" + start + "&end=" + end
    content = requests.get(url).text
    data = json.loads(content)
    return get_average_value(data["bpi"].values())


def get_average_value(vals):
    tot = 0
    for v in vals:
        tot += v
    return tot / len(vals)


def get_latest_btc_price():
    url = "https://api.coindesk.com/v1/bpi/currentprice.json"
    content = requests.get(url).text
    data = json.loads(content)
    return data["bpi"]["USD"]["rate_float"]


def get_btc_ath():
    today = datetime.today()
    start = datetime.fromtimestamp(today.timestamp()-60*60*24*200)
    url = "https://api.coindesk.com/v1/bpi/historical/close.json?start=" + start.strftime('%Y-%m-%d') + "&end=" + today.strftime('%Y-%m-%d')
    content = requests.get(url).text
    data = json.loads(content)
    return get_top(data["bpi"].values())


def get_top(values):
    top = 0
    for i in values:
        if i > top:
            top = i
    return top


def get_btc_realized_cap():  # MM
    return 0


def get_stddev(btc_market_cap):  # standard deviation
    return btc_market_cap


def get_btc_market_cap():
    url = "https://coinmarketcap.com/en/currencies/bitcoin/"
    content = requests.get(url).text
    soup = BeautifulSoup(content, features="html.parser")
    for b in soup.find_all("div"):
        # h5 mb-0 font-weight-bold text-gray-800
        if "text-gray-800" in str(b.get("class")):
            return float(b.string.split("$")[1].replace(",", ""))

    return 0


def get_btc_total_supply():
    url = "https://coinranking.com/coin/Qwsogvtv82FCd+bitcoin-btc"
    content = requests.get(url).text
    soup = BeautifulSoup(content, features="html.parser")
    for b in soup.find_all("abbr"):
        # h5 mb-0 font-weight-bold text-gray-800
        if "abbr--clickable" in str(b.get("class")) and "18,6" in str(b.get("title")):
            return float(b.get("title").replace(",", "")[:-3]) - 1000000


def get_mined_btc_for(nb_days):
    before_halving_mining = 12.5
    after_halving_mining = before_halving_mining / 2
    halving_timestamp = 1589223000
    today = datetime.today().timestamp()
    nb_days_since_halving = (today - halving_timestamp) / (60 * 60 * 24)
    if nb_days_since_halving > nb_days:
        return nb_days * 6*24 * after_halving_mining
    nb_mined_btc_since_halving = nb_days_since_halving * 6 * 24 * after_halving_mining
    nb_mined_btc_before_halving = (nb_days - nb_days_since_halving) * 6 * 24 * before_halving_mining
    return nb_mined_btc_since_halving + nb_mined_btc_before_halving
