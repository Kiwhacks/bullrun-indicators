from indicators.tools import *
from indicators.colors import *


def get_wma200_heatmap():
    today = datetime.today()
    begin_w_200 = datetime.fromtimestamp(today.timestamp()-60*60*24*7*200)
    end_m = datetime.fromtimestamp(today.timestamp()-60*60*24*7*4)
    begin_w_200_m = datetime.fromtimestamp(today.timestamp()-60*60*24*7*(200+4))

    ma_200 = get_moving_average(begin_w_200.strftime('%Y-%m-%d'), today.strftime('%Y-%m-%d'))
    ma_200_m = get_moving_average(begin_w_200_m.strftime('%Y-%m-%d'), end_m.strftime('%Y-%m-%d'))
    return ((ma_200/ma_200_m) - 1) * 100


def get_wma200_heatmap_color(wma200):
    if wma200 < 10:
        return green
    if wma200 < 12:
        return yellow
    if wma200 < 14:
        return orange
    return red
