from indicators.tools import *
from indicators.colors import *


def get_pi_indicator():
    today = datetime.today()
    begin_350 = datetime.fromtimestamp(today.timestamp()-60*60*24*350)
    begin_111 = datetime.fromtimestamp(today.timestamp()-60*60*24*111)

    ma_350_2 = get_moving_average(begin_350.strftime('%Y-%m-%d'), today.strftime('%Y-%m-%d')) * 2
    ma_111 = get_moving_average(begin_111.strftime('%Y-%m-%d'), today.strftime('%Y-%m-%d'))
    return ma_350_2/ma_111


def get_pi_indicator_color(pi_indicator):
    if pi_indicator > 1.10:
        return green
    if pi_indicator > 1.06:
        return yellow
    if pi_indicator > 1.02:
        return orange
    return red
