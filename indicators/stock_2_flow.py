from indicators.tools import *
from indicators.colors import *


def get_stock_2_flow(days):
    supply = get_btc_total_supply()
    nb_btc_mined = get_mined_btc_for(days)
    stock_2_flow = supply / ((nb_btc_mined/days)*360)
    return __import__("math").exp(-1.84) * stock_2_flow ** 3.36


def get_stock_2_flow_colors(btc_current_value, stock_2_flow_10, stock_2_flow_463):
    s2f_10_color = green
    s2f_463_color = green

    if btc_current_value > stock_2_flow_10 * 1.05:
        s2f_10_color = red
    elif btc_current_value > stock_2_flow_10:
        s2f_10_color = orange
    elif btc_current_value > stock_2_flow_10 * 0.95:
        s2f_10_color = yellow

    if btc_current_value > stock_2_flow_463 * 1.05:
        s2f_463_color = red
    elif btc_current_value > stock_2_flow_463:
        s2f_463_color = orange
    elif btc_current_value > stock_2_flow_463 * 0.95:
        s2f_463_color = yellow

    return [purple, s2f_10_color, s2f_463_color]
