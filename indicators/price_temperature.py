from indicators.tools import *
from indicators.colors import *


def get_price_temperature():
    actual_price = get_latest_btc_price()
    today = datetime.today()
    begin_4_years = datetime.fromtimestamp(today.timestamp()-60*60*24*365*4)
    ma_4_y = get_moving_average(begin_4_years.strftime('%Y-%m-%d'), today.strftime('%Y-%m-%d'))
    return actual_price/ma_4_y - 1


def get_price_temperature_color(price_temperature):
    if price_temperature < 4.5:
        return green
    if price_temperature < 6:
        return yellow
    if price_temperature < 7.6:
        return orange
    return red
